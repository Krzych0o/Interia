class WelcomeController < ApplicationController
  def index
  end

  def parse
    logger = Logger.new("#{Rails.root}/log/interia.log")
    job_parser = Services::JobParser.new(logger)
    @result = job_parser.call(params[:job_structure])
  end
end
