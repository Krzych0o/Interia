module Services
  class JobParser
    def initialize(logger)
      @logger = logger
    end

    def call(data)

      @job_array = convert_to_array(data)
      search_recursively

    end

    private
    def convert_to_array(data)
      jobs = []
      lines = data.split(/[\r\n]+/)
      lines.each do |line|
        jobs << line.split(/( )=>/).map{|elem| elem.strip}.reject { |e| e.to_s.strip.empty? }
      end
      jobs
    end

    def search_recursively
      @result = []
      @job_array.each do |job|
        if @result.select{|el| el.include? job.first.to_s}.empty?
          jobs = find_child(job)
          @logger.info jobs
          if @result.select{|el| el.include? job.second.to_s}.empty?
            @result << jobs.flatten.join('').reverse
          else
            @result << jobs.first.reverse
          end
        end
      end

      @result.join('')
    end

    def find_child(job)
      return [job.first] if job.second.nil?

      next_element = @job_array.select{|elem| elem.first.to_s == job.second.to_s}
      child = find_child(next_element)
      [job.first, child]
    end

    attr_reader :job_array, :result
  end
end